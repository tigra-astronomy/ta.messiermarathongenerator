# Messier Marathon Generator for ACP Expert Scheduler #

This simple utility is based loosely on the MessierMarathon.js script that ships with ACP Expert Scheduler.
We have simply converted it to C# and added a few features such as repeat counts and schedule constraints.

### How do I get set up? ###

Download the code and edit the constants defined at the top of `Program.cs` to your liking.
You will need to reference the files `RTML.dll` and `LiquidTechnologies.Runtime.Net35.dll` from the 
ACP Expert Scheduler program directory, which is normally at `C:\Program Files (x86)\ACP Scheduler`. 
These files are not licensed for redistribution so you must reference them from your local Scheduler install.

Build the code and run it. You should see RTML output scroll up the screen.

Save the output to a file by redirecting the condole output to a file:  

`TA.MessierMarathonGenerator.exe >GeneratedProject.rtml`

You can then import the generated RTML file into your Scheduler database using the `Import RTML` option, 
under the `Data` menu, in the Schedule Browser utility.

### To Do List ###

This was not a difficult piece of code to write and there is lots of room for improvement. Almost anyone could pick up this code and add features or improvements to it. Please feel free to have a go! We love pull requests.

The following items are just some ideas for enhancement that I came up with in about 10 seconds flat. You may have other ideas, so by all means knock yourself out...  

* Add a command line interface for setting options rather than requiring the code to be edited
* Support additional schedule constraints
* Take a target list from an input file (CSV, tab separated, etc.)


### Licensing and Ownership ###

This project is licensed under the [Tigra MIT License](http://tigra.mit-license.org "Tigra MIT Open Source License"). In summary, "anyone can do anything at all with this software without restriction. Whatever happens is not my fault".

Any code you commit to this repository irrevocably becomes part of the project and is licensed under the Tigra MIT License.


Tim Long  
Tigra Astronomy  
December 2016