﻿// This file is part of the TA.MessierMarathonGenerator project
// 
// Copyright © 2016-2016 Tigra Astronomy., all rights reserved.
// 
// File: Program.cs  Last modified: 2016-12-29@18:05 by Tim Long

using System;
using System.Collections.Generic;
using DC3.RTML23;
using TA.ObjectOrientedAstronomy.FundamentalTypes;
using static DC3.RTML23.Enumerations;

/*
 * Licensed under the MIT License. See http://tigra.mit-license.org
 */

namespace TA.MessierMarathonGenerator
    {
    internal class Program
        {
        /*
         * Change these constants to your liking.
         * Note: MoonDown and MoonAvoid are mutually exclusive in RTML even though Scheduler allows both.
         * Therefore if you set UseMoonAvoidLorentzian to true, no Moon Down constraint
         * will be generated.
         */
        private const uint TargetRepeatCount = 1;
        private const uint ImageSetRepeatCount = 1;
        private const bool MoonMustBeDown = false;
        private const bool UseMoonAvoidLorentzian = true;
        private const float MoonAvoidWidthDays = 12;
        private const float MoonAvoidDistanceDegrees = 120;
        private const int ExposureSeconds = 60;
        private const string Filter = null; // (string) filter name or (null) for unfiltered
        private const string ProjectName = "Messier Marathon Unguided Pilot Images";
        private const string ContactUser = "Tim Long [ACP Tim]";
        private const string ContactEmail = "Tim@tigra-astronomy.com";
        private const string ProjectOrganization = "Tigra Astronomy";

        private static void Main(string[] args)
            {
            var RTML = new RTML
                {
                Version = RTML_Version.V23,
                Contact = new Contact
                    {
                    User = ContactUser,
                    Email = ContactEmail,
                    Organization = ProjectOrganization
                    }
                };
            var targets = GetTargets();
            var requests = GenerateRequests(targets);
            foreach (var request in requests)
                {
                RTML.Requests.Add(request);
                }
            var xmlString = RTML.ToXml();
            Console.Write(xmlString);
            Console.WriteLine();
#if DEBUG
            Console.ReadLine();
#endif
            }

        private static IEnumerable<Request> GenerateRequests(IEnumerable<Target> targets)
            {
            var rtmlTargets = new List<Request>();
            foreach (var target in targets)
                {
                rtmlTargets.Add(CreatePlan(target));
                }
            return rtmlTargets;
            }

        private static Request CreatePlan(Target target)
            {
            var observation = CreateObservation(target);
            var plan = new Request
                {
                Project = ProjectName,
                UserName = ContactUser,
                ID = target.Name,
                Description =
                    $"Auto-generated plan with single observation of {target.Name}, repeated {TargetRepeatCount} times."
                };
            plan.Targets.Add(observation);
            plan.Schedule = CreateSchedule(target);
            return plan;
            }

        private static Schedule CreateSchedule(Target target)
            {
            var moonConstraints = new Moon
                {
                Down = MoonMustBeDown
                };
            if (UseMoonAvoidLorentzian)
                {
                var moonAvoid = new Lorentzian
                    {
                    Distance = MoonAvoidDistanceDegrees,
                    Width = MoonAvoidWidthDays
                    };
                moonConstraints.Lorentzian = moonAvoid;
                }
            var schedule = new Schedule
                {
                Moon = moonConstraints
                };
            return schedule;
            }

        private static DC3.RTML23.Target CreateObservation(Target target)
            {
            var targetCoordinates = new Coordinates
                {
                // RTML requires RA in degrees, not hours.
                RightAscension = target.RightAscension.Value * 15.0,
                Declination = target.Declination.Value
                };
            var targetType = new TargetType {Coordinates = targetCoordinates};
            var observation = new DC3.RTML23.Target
                {
                Count = TargetRepeatCount,
                TargetType = targetType,
                Name = target.Name, // Observation name is object catalogue name
                Description = $"Auto-generated observation for {target.Name}, repeated {TargetRepeatCount} times"
                };
            observation.Pictures.Add(CreateImageSet(target));
            return observation;
            }

        private static Picture CreateImageSet(Target target)
            {
            var exposureSpec = new ExposureSpec {ExposureTime = ExposureSeconds};
            var filterDescription = Filter == null ? "unfiltered" : $"through the {Filter} filter";
            var imageSet = new Picture
                {
                Name = Filter == null ? "Unfiltered" : Filter + " filter",
                ExposureSpec = exposureSpec,
                Filter = Filter ?? string.Empty,
                Count = ImageSetRepeatCount,
                Description =
                    $"{ImageSetRepeatCount} images of {ExposureSeconds} seconds, total {ImageSetRepeatCount * ExposureSeconds} seconds, {filterDescription}"
                };
            return imageSet;
            }

        private static IEnumerable<Target> GetTargets()
            {
            var targetList = new List<Target>
                {
                new Target("M1", 5.575, 22.017),
                new Target("M10", 16.9517, -4.1),
                new Target("M100", 12.3817, 15.817),
                new Target("M101", 14.0533, 54.35),
                new Target("M102", 15.1083, 55.767),
                new Target("M103", 1.5567, 60.65),
                new Target("M104", 12.6667, -11.617),
                new Target("M105", 10.7967, 12.583),
                new Target("M106", 12.3167, 47.3),
                new Target("M107", 16.5417, -13.05),
                new Target("M108", 11.1917, 55.667),
                new Target("M109", 11.96, 53.367),
                new Target("M11", 18.8517, -6.267),
                new Target("M110", 0.6733, 41.683),
                new Target("M12", 16.7867, -1.95),
                new Target("M13", 16.695, 36.467),
                new Target("M14", 17.6267, -3.25),
                new Target("M15", 21.5, 12.167),
                new Target("M16", 18.3133, -13.783),
                new Target("M17", 18.3467, -16.183),
                new Target("M18", 18.3317, -17.133),
                new Target("M19", 17.0433, -26.267),
                new Target("M2", 21.5583, -0.817),
                new Target("M20", 18.045, -22.967),
                new Target("M21", 18.07, -22.483),
                new Target("M22", 18.6067, -23.9),
                new Target("M23", 17.9517, -18.983),
                new Target("M24", 18.4433, -18.383),
                new Target("M25", 18.53, -19.117),
                new Target("M26", 18.7533, -9.4),
                new Target("M27", 19.9933, 22.717),
                new Target("M28", 18.4083, -24.867),
                new Target("M29", 20.3983, 38.533),
                new Target("M3", 13.7033, 28.383),
                new Target("M30", 21.6733, -23.183),
                new Target("M31", 0.7117, 41.267),
                new Target("M32", 0.7117, 40.867),
                new Target("M33", 1.565, 30.65),
                new Target("M34", 2.7017, 42.783),
                new Target("M35", 6.1367, 24.367),
                new Target("M36", 5.605, 34.133),
                new Target("M37", 5.8717, 32.55),
                new Target("M38", 5.4783, 35.85),
                new Target("M39", 21.5367, 48.433),
                new Target("M4", 16.3933, -26.533),
                new Target("M40", 12.365, 58.1),
                new Target("M41", 6.7667, -20.75),
                new Target("M42", 5.5883, -5.383),
                new Target("M43", 5.5917, -5.267),
                new Target("M44", 8.6667, 19.667),
                new Target("M45", 3.7833, 24.117),
                new Target("M46", 7.6967, -14.817),
                new Target("M47", 7.61, -14.483),
                new Target("M48", 8.2283, -5.75),
                new Target("M49", 12.4967, 8),
                new Target("M5", 15.31, 2.083),
                new Target("M50", 7.0417, -8.383),
                new Target("M51", 13.4983, 47.2),
                new Target("M52", 23.4033, 61.583),
                new Target("M53", 13.215, 18.167),
                new Target("M54", 18.9183, -30.483),
                new Target("M55", 19.6667, -30.967),
                new Target("M56", 19.2767, 30.183),
                new Target("M57", 18.8933, 33.033),
                new Target("M58", 12.6283, 11.817),
                new Target("M59", 12.7, 11.65),
                new Target("M6", 17.6717, -32.25),
                new Target("M60", 12.7283, 11.55),
                new Target("M61", 12.365, 4.467),
                new Target("M62", 17.02, -30.117),
                new Target("M63", 13.2633, 42.033),
                new Target("M64", 12.945, 21.683),
                new Target("M65", 11.315, 13.083),
                new Target("M66", 11.3367, 13),
                new Target("M67", 8.8467, 11.817),
                new Target("M68", 12.6583, -26.75),
                new Target("M69", 18.5233, -32.35),
                new Target("M7", 17.8983, -34.8),
                new Target("M70", 18.72, -32.3),
                new Target("M71", 19.8967, 18.783),
                new Target("M72", 20.8917, -12.533),
                new Target("M73", 20.9833, -12.633),
                new Target("M74", 1.6117, 15.783),
                new Target("M75", 20.1017, -21.917),
                new Target("M76", 1.705, 51.583),
                new Target("M77", 2.7117, -0.017),
                new Target("M78", 5.78, 0.083),
                new Target("M79", 5.4033, -24.517),
                new Target("M8", 18.0617, -24.383),
                new Target("M80", 16.2833, -22.983),
                new Target("M81", 9.9267, 69.067),
                new Target("M82", 9.9317, 69.683),
                new Target("M83", 13.6167, -29.867),
                new Target("M84", 12.4183, 12.883),
                new Target("M85", 12.4233, 18.183),
                new Target("M86", 12.4367, 12.95),
                new Target("M87", 12.5133, 12.383),
                new Target("M88", 12.5333, 14.417),
                new Target("M89", 12.595, 12.55),
                new Target("M9", 17.32, -18.517),
                new Target("M90", 12.6133, 13.167),
                new Target("M91", 12.59, 14.5),
                new Target("M92", 17.285, 43.133),
                new Target("M93", 7.7417, -23.85),
                new Target("M94", 12.8483, 41.117),
                new Target("M95", 10.7333, 11.7),
                new Target("M96", 10.78, 11.817),
                new Target("M97", 11.2467, 55.017),
                new Target("M98", 12.23, 14.9),
                new Target("M99", 12.3133, 14.417)
                };
            return targetList;
            }
        }

    internal class Target
        {
        public Target(string name, double rightAscension, double declination)
            {
            Name = name;
            RightAscension = new RightAscension(rightAscension);
            Declination = new Declination(declination);
            }

        public string Name { get; set; }

        public RightAscension RightAscension { get; set; }

        public Declination Declination { get; set; }
        }
    }